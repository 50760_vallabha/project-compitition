
import java.util.*;
class Solution3{

    public int space(final String s){
        String x = s.trim();
        int len = 0;
        int n = x.length();
        for(int i=0; i<n; i++){
            if(x.charAt(i) == ' ')
            len = 0;

            else
            len++;
        }
        return len;
    }

    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        System.out.println("enter a string");
        String s = sc.nextLine();
        Solution3 obj = new Solution3();

        System.out.println(obj.space(s));



    }
}
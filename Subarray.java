import java.util.*;
class Subarray{


    public int sub(int[]arr, int n){
        int s =0, e = n-1;
        while(s < n-1 && arr[s]<=arr[s+1]) s++;
        if(s == n-1) return 0;

        while(e >= s && arr[e] >= arr[e-1]) e--;
        if(e == 0) return n-1;

        int result = Math.min(n - 1 -s, e);

        int i =0, j = e;
        while(i<= s && j<n){
            if(arr[j]>=arr[i]){
                result = Math.min(result, j-i-1);
                i++;
            }
            else;{
                j++;
            }
        }
        return result;
    }




    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        System.out.println("no of array array elements");
        int n = sc.nextInt();

        System.out.println("enter array elements");
        int arr[] = new int[n];

        for(int i =0; i<n; i++){
           arr[i] = sc.nextInt();
        }
        int x = arr.length;

           Subarray m = new Subarray();

            System.out.println("answer is  "+m.sub(arr, x));

        
    }
          
    
}


//5,4,3,2,1
//1,2,3,10,4,2,3,5